# RUNS #

This is an attempt to learn statistical analysis using R, by the means of analyzing the batting records of international cricketers.

At present this project is able to generate reports for the following two batsmen:  
- Rahul Dravid: http://www.ics.uci.edu/~vpalepu/data/cricket/report-dravid.html  
- Ricky Ponting: http://www.ics.uci.edu/~vpalepu/data/cricket/report-ponting.html  
- Sachin Tendulkar: http://www.ics.uci.edu/~vpalepu/data/cricket/report-tendulkar.html

### How do I get set up? ###

* Summary of set up
- Clone this Repository using Git
- Install R and R Studio
- Open the project in R Studio
- Hit "Source" or "Knit HTML" to run the analyes and generate the reports. 

### Contribution guidelines ###

* Data Collection
* Additional Analysis

### Who do I talk to? ###

* Vijay Krishna Palepu twitter: [@<space>vkrishnapalepu]