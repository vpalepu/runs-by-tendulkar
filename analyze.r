## @knitr init
# This code initializes the data.
odiData <- read.csv(file="batters-dravid-odis.csv", header=TRUE, sep=",")
odiRuns <- na.omit(odiData$Runs)

testsData <- read.csv(file="batters-dravid-tests.csv", header=TRUE, sep=",")
testsRuns <- na.omit(testsData$Runs)


## @knitr printSummaryStats
printRunsStats <- function(runs, label, colr, maxruns){
  print(label)
  print(mean(runs, na.rm=TRUE))
  print(median(runs), na.rm=TRUE)
  
  hist(runs, plot=TRUE, breaks=pretty(1:maxruns, n = maxruns/10), 
       labels=TRUE, main=paste("Histogram of the Runs scored in ", label), 
       xlab="Runs Scored", ylab="# of Innings",
       ylim=c(0,135),
       col = colr, border = "white")  
}

if(max(testsRuns) > max(odiRuns)) {
  maxruns <- max(testsRuns) + 10  
} else {
  maxruns <- max(odiRuns) + 10
}

printRunsStats(odiRuns, "ODIs", "royalblue", maxruns)
printRunsStats(testsRuns, "Tests", "red", maxruns)

## @knitr runDistributions
runDistributions <- function (odiRuns, testsRuns) {
  boxplot(odiRuns, testsRuns, notch=TRUE, add=FALSE, at=1:2, 
          names=c("ODI Innings", "Test Innings"), ylab="Runs Scored",
          col=c("royalblue", "red"), boxwex = 0.2)
}

runDistributions(odiRuns, testsRuns)

## @knitr runDistributionsByYear
runsByYear <- function(odiData, testsData) {
  old.par <- par(mfrow = c(2, 1), cex=0.6, mar = c(4, 4, 2, 1))
  
  odiYears <- as.numeric(format(as.Date(odiData$Date, 
                                        format="%d %b %y"), "%Y"))
  boxplot(odiData$Runs ~ odiYears, notch=FALSE, add=FALSE,
          ylab="Runs Scored", las=2, col="royalblue",
          main="Distribution of ODI Runs scored each Year")
  
  testYears <- as.numeric(format(as.Date(testsData$Start.Date, 
                                         format="%d %b %Y"), "%Y"))
  boxplot(testsData$Runs ~ testYears, notch=FALSE, add=FALSE,
          ylab="Runs Scored", las=2, col="red",
          main="Distribution of Test Runs scored each Year")
  
  par(old.par)
}

runsByYear(odiData, testsData)

## @knitr runDistributionsByCountry
runDistributionsByCountry <- function(odiData, testsData) {
  old.par <- par(mfrow = c(2, 1), cex=0.6, mar = c(10, 4, 1, 1))
  boxplot(odiData$Runs ~ odiData$Opposition, notch=FALSE, add=FALSE, 
          ylab="Runs Scored", las=2, col="royalblue")
  par(mar = c(7, 4, 1, 1))
  boxplot(testsData$Runs ~ testsData$Opposition, notch=FALSE, add=FALSE,
          ylab="Runs Scored", las=2, col="red")
  
  par(old.par)
}

runDistributionsByCountry(odiData, testsData)

## @knitr runDistributionsByBattingPosition
runDistributionsByBattingPosition <- function (odiData, testsData) {
  old.par <- par(mfrow = c(2, 1), cex=0.6, mar = c(5, 4, 2, 2))
  boxplot(odiData$Runs ~ odiData$Batting.Position, notch=FALSE, add=FALSE, 
          ylab="Runs Scored", xlab="Batting Positions",
          col="royalblue", boxwex=0.2, ylim=c(0, 250), las = 1,
          main="Runs Scored @ various Batting Positions")
  
  boxplot(testsData$Runs ~ testsData$Pos, notch=FALSE, add=FALSE,
          ylab="Runs Scored", xlab="Batting Positions",
          col="red", boxwex=0.3, ylim=c(0, 250), las = 1,
          main="Runs Scored @ various Batting Positions")
  
  par(old.par)
}

runDistributionsByBattingPosition(odiData, testsData);

## @knitr runDistributionsByInnings
runDistributionsByInnings <- function (odiData, testsData) {
  old.par <- par(mfrow = c(2, 1), cex=0.6, mar = c(5, 4, 1, 1))
  
  boxplot(odiData$Runs ~ odiData$Inns, notch=FALSE, add=FALSE, 
          ylab="Runs Scored", xlab="Innings", 
          col="royalblue", boxwex=0.2, ylim=c(0, 250))
  
  reg1 <- lm(odiData$Runs ~ odiData$Inns)
  abline(reg1)
  
  boxplot(testsData$Runs ~ testsData$Inns, notch=FALSE, add=FALSE,
          ylab="Runs Scored", xlab="Innings", 
          col="red", boxwex=0.2, ylim=c(0, 250))
  
  reg1 <- lm(testsData$Runs ~ testsData$Inns)
  abline(reg1)
  
  
  par(old.par)
}

runDistributionsByInnings(odiData, testsData);

## @knitr runDistributionsByDismissalType
runDistributionsByDismissalType <- function (odiData, testsData) {
  old.par <- par(mfrow = c(2, 1), cex=0.6, mar = c(7, 4, 1, 1))
  boxplot(odiData$Runs ~ odiData$Dismissal, notch=FALSE, add=FALSE, 
          ylab="Runs Scored", las=2, col="royalblue")
  par(mar = c(5, 4, 1, 1))
  boxplot(testsData$Runs ~ testsData$Dismissal, notch=FALSE, add=FALSE,
          ylab="Runs Scored", las=2, col="red")
  
  par(old.par)
}

runDistributionsByDismissalType(odiData, testsData);

## @knitr runDistributionsByGround
runDistributionsByGround <- function (odiData, testsData) {
  old.par <- par(cex=0.4, mar = c(10, 5, 1, 1), pin = c(8, 10), pty="m")
  print("Open Image in a new Tab to Zoom in...")
  boxplot(odiData$Runs ~ odiData$Ground, notch=FALSE, add=FALSE, 
          ylab="Runs Scored", col="royalblue", boxwex=0.6,
          ylim=c(0, 250), las = 2,
          medcol = "white")
  
  print("Open Image in a new Tab to Zoom in...")  
  boxplot(testsData$Runs ~ testsData$Ground, notch=FALSE, add=FALSE,
          ylab="Runs Scored", col="red", boxwex=0.6, ylim=c(0, 250), las = 2,
          horizontal = FALSE, medcol = "white")
  
  par(old.par)
}

runDistributionsByGround(odiData, testsData);



## @knitr runsPerMatch
runsPerMatch <- function (odiData, testsData) {
  old.par <- par(mfrow = c(2, 1), cex=0.6, mar = c(6, 4, 1, 1), bg = 240)
  barplot(rev(odiData$Runs), names.arg=rev(odiData$Date), 
          xlab="Dates for ODI Matches", ylab="ODI Runs Scored",
          main="Runs Scored in every ODI Cricket Match (chronologically)",
          col="royalblue", border="NA")
  abline(h=50, col = "white")
  abline(h=100, col = "white")
  abline(h=150, col = "white")
  abline(h=200, col = "white")
  
  barplot(rev(testsData$Runs), names.arg=rev(testsData$Start.Date), 
          xlab="Start Dates for Test Matches", ylab="Test Runs Scored",
          main="Runs Scored in every Test Cricket Match (chronologically)",
          col="red", border="NA")
  abline(h=50, col = "white")
  abline(h=200, col = "white")
  abline(h=100, col = "white")
  abline(h=150, col = "white")
  abline(h=200, col = "white")
  
  par(old.par)
  
}

runsPerMatch(odiData, testsData);


## @knitr teamwiseRunsPerODIMatch
runsEveryMatchVsTeams <- function (odiData, isODI) {
  old.par <- par(mfrow = c(3, 3), cex=0.6, mar = c(5, 4, 1, 1))
  odiTeams <- sort(unique(odiData$Opposition))
  odiTeamCount <- length(odiTeams)
  
  maxbars <- -1
  for(i in 1:odiTeamCount) {
    team <- odiTeams[i]
    dataVsTeam  <- subset(odiData, Opposition==team)
    matchCount <- length(dataVsTeam$Runs)
    if(matchCount > maxbars) {
      maxbars <- matchCount
    }
  }
  
  for(i in 1:odiTeamCount) {
    team <- odiTeams[i]
    dataVsTeam  <- subset(odiData, Opposition==team)
    runs <- rev(dataVsTeam$Runs)
    
    if(isODI == TRUE) {
      dates <- rev(dataVsTeam$Date)
      xlabl <- "Dates for ODI Match(es) vs. "
      colr = "royalblue"
    } else {
      dates <- rev(dataVsTeam$Start.Date)
      xlabl <- "Start Dates for Test Match(es) vs. "
      colr = "red"
    }
    
    
    barplot(runs, names.arg=dates, 
            xlab=paste(xlabl, team), 
            ylab="ODI Runs Scored",
            ylim=c(0,250), 
            xlim=c(0,maxbars),
            density=runs, col=colr, width=0.8)
  }
  
  par(old.par)
}

runsEveryMatchVsTeams(odiData, TRUE);

## @knitr teamwiseRunsPerTestMatch
runsEveryMatchVsTeams(testsData, FALSE);


## @knitr sixesAndFours
sixesAndFours <- function (odiData, testsData) {
  old.par <- par(mfrow = c(2, 2), cex=0.6, mar = c(5, 4, 2, 2))
  boxplot(odiData$Runs ~ odiData$X4s, 
          xlab="# of Fours", ylab = "ODI Runs Scored", col="royalblue")
  reg1 <- lm(odiData$Runs ~ odiData$X4s)
  abline(reg1)
  boxplot(odiData$Runs ~ odiData$X6s, 
          xlab="# of Sixes", ylab = "ODI Runs Scored", col="royalblue")
  reg1 <- lm(odiData$Runs ~ odiData$X6s)
  abline(reg1)
  boxplot(testsData$Runs ~ testsData$X4s, 
          xlab="# of Fours", ylab = "Test Runs Scored", col="red")
  reg1 <- lm(testsData$Runs ~ testsData$X4s)
  abline(reg1)  
  boxplot(testsData$Runs ~ testsData$X6s, 
          xlab="# of Sixes", ylab = "Test Runs Scored", col="red")
  reg1 <- lm(testsData$Runs ~ testsData$X6s)
  abline(reg1)
  par(old.par)
}

sixesAndFours(odiData, testsData)

sixesAndFours2 <- function (odiData, testsData) {
  old.par <- par(mfrow = c(2, 2), cex=0.6, mar = c(5, 4, 2, 2))
  plot(odiData$X4s, odiData$Runs,
       xlab="# of Sixes", ylab = "ODI Runs Scored", col="royalblue")
  reg1 <- lm(odiData$Runs ~ odiData$X4s)
  abline(reg1)
  plot(odiData$X6s, odiData$Runs, 
          xlab="# of Sixes", ylab = "ODI Runs Scored", col="royalblue")
  reg1 <- lm(odiData$Runs ~ odiData$X6s)
  abline(reg1)
  plot(testsData$X4s, testsData$Runs,
          xlab="# of Fours", ylab = "Test Runs Scored", col="red")
  reg1 <- lm(testsData$Runs ~ testsData$X4s)
  abline(reg1)
  plot(testsData$X6s, testsData$Runs, 
          xlab="# of Sixes", ylab = "Test Runs Scored", col="red")
  reg1 <- lm(testsData$Runs ~ testsData$X6s)
  abline(reg1)
  par(old.par)
}

sixesAndFours2(odiData, testsData)